<?php include("vars.php") ?>

<html>
    <head>
        <title><?=$title?></title>
        <meta charset="utf-8" />
        <link rel="stylesheet" type="text/css" href="<?=$resourceDir?>/style.css" />
        <link rel="icon" href="<?=$resourceDir?>/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="<?=$resourceDir?>/favicon.ico" type="image/x-icon" />
    </head>
    <body>
        <?php
        function loadPage($location) {
            global $pageDir, $error401page, $error404page;

            $dirPath = realpath(dirname("$pageDir/$location"));
            $base = getcwd()."/$pageDir";

            if (substr($dirPath, 0, strlen($base)) == $base) {
                if (file_exists("$pageDir/$location")) {
                    include("$pageDir/$location");
                } else {
                    include("$pageDir/$error404page");
                }
            } else {
                include("$pageDir/$error401page");
            }
        }

        loadPage($headerPage);

        if (isset($_GET[$pageArg])) {
            if ($_GET[$pageArg] == '') {
                $page = $mainPage;
            } else {
                $page = $_GET[$pageArg];
            }
        } else {
            $page = $mainPage;
        }

        loadPage($page);

        loadPage($footerPage);
        ?>
        <script src="<?=$resourceDir?>/script.js"></script>
    </body>
</html>