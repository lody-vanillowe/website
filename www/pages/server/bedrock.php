<h2>Lody Bedrockowe</h2>

<p>
    Lody Bedrockowe to subserwer na wersję Bedrock Edition gry. Zasady są te same: SMP dla znajomych. Mimo, że prawie żaden z graczy Lodów nie jest regularnym użytkownikiem Bedrocka, jest kilka powodów dla których ten serwer istnieje. Po pierwsze - wiek mapy i glitche. Zaczynając w momencie, w którym Bedrock jest zabugowany, mamy szansę mieć w przyszłości unikalną mapę z wieloma niemożliwymi w przyszłości rzeczami. Po drugie - trzymanie pod pachą laptopa w każdym momencie jest mniej wygodne niż trzymanie w kieszeni Switcha czy smartfona, więc Bedrockowe lepiej nadają się do "przypadkowego" grania.
</p>

<h3>DNS</h3>
<p>
    Z racji, że wersja na Nintendo Switcha nie ma możliwości dołączenia przy użyciu IP <i>"ze względu na ograniczenia platformy"</i> (jasne), postanowiłem postawić serwer DNS (pod portem <code>32</code>). Jedyne co musisz zrobić, aby zagrać tu na swoim Switchu, to zmienić DNS na numeryczny adres IP serwera (<code><?=file_get_contents('https://api.ipify.org')?></code>). Jak to zrobić, dowiesz się <a href="https://en-americas-support.nintendo.com/app/answers/detail/a_id/22411/~/how-to-manually-enter-dns-settings" target="_blank">tutaj</a>. Następnie odpal Minecrafta i wybierz serwer "Hive". Zostaniesz połączony z Bedrockowymi. Powodzenia!
</p>

<p>
    <b>UWAGA:</b> Numeryczne IP zmienia się co jakiś czas. Jeśli DNS nie działa, wejdź na tą stronę i sprawdź je ponownie, bądź użyj narzędzia takiego jak <code>ping</code> lub <code>nslookup</code>.
</p>

<h3><a href="index.php?p=bedrock_map.html">Mapa Lodów Bedrockowych</a></h3>

<h3>Adres IP</h3>
<code>stary.pc.pl:19132</code>